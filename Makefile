DEST ?= /usr/local/bin
CONF ?= /etc
MANP ?= /usr/local/man/man1

all:
	@echo Run \'make install\' to install homesnapshot

install:
	@mkdir -p $(DEST)
	@mkdir -p $(MANP)
	@install --mode=0755 homesnapshot $(DEST)/
	@install --mode=0644 homesnapshot.conf $(CONF)/
	@install --mode=0644 doc/homesnapshot.1 $(MANP)/
	@gzip $(MANP)/homesnapshot.1
	@echo homesnapshot has been installed

uninstall:
	@rm $(DEST)/homesnapshot
	@rm $(CONF)/homesnapshot.conf
	@rm $(MANP)/homesnapshot.1.gz
	@echo homesnapshot has been removed
