## homesnapshot
This script creates rolling snapshots of the user's home directory using rsync. It does this in a very space efficient way; by sharing common files among the snapshots with hard links.  

_homesnapshot_ is useful for finding previous versions of files, restoring after reinstall, or disaster recovery.   

Snapshots can be sent to storage mounted locally or remotely via ssh.  
See the [wiki](https://gitlab.com/justsudoit/homesnapshot/-/wikis/home) for more information.

#### Example:
User **'user'** whose home folder is **'/home/user'**  
Their snapshot location is set to **'/mnt/storage'**  
They have chosen to keep **3** snapshots, and they make a snapshot every day at 12 o'clock  
After creating their first snapshot, the contents of **'/mnt/storage'** appear as follows:  

        $ ls /mnt/storage  
        user:2020-01-01-12:00  

        $ ls /mnt/storage/user:2020-01-01-12:00 
        Desktop Documents Pictures Templates Videos  
 
After creating 3 snapshots, they would see:  

        $ ls /mnt/stroage
        user:2020-01-01-12:00  
        user:2020-01-02-12:00  
        user:2020-01-03-12:00  
After creating a fourth, the oldest snapshot would be removed  

        $ ls /mnt/storage 
        user:2020-01-02-12:00  
        user:2020-01-03-12:00  
        user:2020-01-04-12:00  
        

### Requirements
- bash
- rsync
- openssh (optional, used for storing snapshots on remote hosts)
- systemd (optional, used for automated snapshots)

### How to install

1. Clone this repo  
`git clone https://gitlab.com/justsudoit/homesnapshot.git`  

1. Make  
`cd homesnapshot`  
`sudo make install`

1. Launch homesnapshot   
`homesnapshot`  
The first time you launch _homesnapshot_, the scripted configurator will start. (See [Configuration](https://gitlab.com/justsudoit/homesnapshot/-/wikis/Configuration) for more info)   

### Uninstall

1. From _homesnapshot_ directory run  
`sudo make uninstall`

### Usage
 - Once you have _homesnapshot_ configured, simply run:  
`homesnapshot`  

 - In order to view the different options available to _homesnapshot_, run:  
`homesnapshot -h`  

- Automatic snapshots will create logs via systemd. You can view the logs with `journalctl -t homesnapshot`  

 - The first snapshot can take a long time depending on the size of your home directory,  
but subsequent runs will take substantially less time, because only changes are being sent.
